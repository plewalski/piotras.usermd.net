"""public_python URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
	1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

admin.autodiscover()

from account import views
from account import urls

urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	url(r'^$', views.home, name='home'),
	url(r'^auction/list/', views.auction_list, name='auction_list'),
	url(r'^auction/create/', views.auction_create, name='auction_create'),
	url(r'^auction/detail/(?P<id>\d+)/$', views.auction_detail, name='auction_detail'),
	url(r'^auction/edit/(?P<id>\d+)/$', views.auction_update, name='auction_update'),
	url(r'^auction/delete/(?P<id>\d+)/$', views.auction_delete, name='auction_delete'),
	#
	url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
	url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
	url(r'^accounts/loggedin/$', views.loggedin, name='loggedin'),
	#
	url(r'^accounts/register/$', views.register, name='register'),
    url(r'^accounts/register/complete/$', views.registration_complete, name='registration_complete'),
    #
	url(r'^about/$', views.about, name='about'),

]
