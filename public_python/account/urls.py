from django.conf.urls import include, url
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^$', views.interview, name='home'),
    url(r'^about/$', views.about, name='about'),
#    url(r'^(?P<id>\d+)/$', views.ad_detail, name='detail'),
#    url(r'^(?P<id>\d+)/edit/$', views.ad_update, name='update'),
#    url(r'^(?P<id>\d+)/delete/$', views.post_delete),
]
