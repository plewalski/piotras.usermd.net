from django.contrib import admin
from account.models import ExpandedUser


class ExpandedUserAdmin(admin.ModelAdmin):
	fieldsets = [
		('User Info', {'fields': ['username', 'first_name', 'last_name', 'email',
								  'password', 'city', 'state', 'zip']}),
		('Date information', {'fields': ['date_joined', 'last_login'], 'classes': ['collapse']}),
		('Permissions', {'fields': ['is_active', 'is_staff', 'is_superuser']}),
	]

	list_filter = ['city']

	list_display = ['username', 'first_name', 'last_name', 'email', 'city', 'state',
					'date_joined', 'last_login']

admin.site.register(ExpandedUser, ExpandedUserAdmin)