from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms.formsets import BaseFormSet

from account.choices import *

class ExpandedUser(AbstractUser):
	city = models.CharField(_("city"), max_length=20, blank=True)
	state = models.CharField(_("state"), max_length=20, blank=True)
	zip = models.CharField(_("zip code"), max_length=5, blank=True)


class Question(models.Model):
	question = models.TextField(blank=True)
	answered = models.BooleanField()

class Recipe(models.Model):
	title = models.CharField(max_length=255)
	description = models.TextField()


class Ingredient(models.Model):
	recipe = models.ForeignKey(Recipe)
	description = models.CharField(max_length=255)


class Instruction(models.Model):
	recipe = models.ForeignKey(Recipe)
	number = models.PositiveSmallIntegerField()
	description = models.TextField()

class TodoList(models.Model):
	name = models.CharField(max_length=100)

	def __unicode__(self):
		return self.name


class TodoItem(models.Model):
	name = models.TextField(help_text="e.g. Buy milk, wash dog etc")
	list = models.ForeignKey(TodoList)

	def __unicode__(self):
		return self.name + " (" + str(self.list) + ")"


class AuctionList(models.Model):
	name = models.CharField(max_length=100)
	title = models.CharField(max_length=1000)
	description = models.TextField()
	table = models.TextField()
	grid = models.IntegerField(choices=GRID_CHOICES, default=2)

	def __unicode__(self):
		return self.name


class AuctionAttr(models.Model):
	
	title = models.CharField(max_length=1000)
	description = models.TextField()
	table = models.TextField()
	

class AuctionItem(models.Model):
	photo = models.CharField(max_length=1000)
	list = models.ForeignKey(AuctionList, related_name="photos")

	def __unicode__(self):
		return self.name + " (" + str(self.list) + ")"