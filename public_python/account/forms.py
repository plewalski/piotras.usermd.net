#forms.py
import re
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from models import *
from django.contrib.auth import get_user_model
User = get_user_model()

class TodoListForm(forms.ModelForm):
	class Meta:
		model = TodoList
		fields = ('name',)


class TodoItemForm(forms.ModelForm):
	class Meta:
		model = TodoItem
		exclude = ('list',)

class AuctionListForm(forms.ModelForm):
	class Meta:
		model = AuctionList
		fields = '__all__'


class AuctionItemForm(forms.ModelForm):
	class Meta:
		model = AuctionItem
		exclude = ('list',)



class QuestionForm(forms.ModelForm):
	class Meta:
		model = Question
		exclude = ('answered',)


 
class RegistrationForm(forms.Form):
 
	username = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Username"), error_messages={ 'invalid': _("This value must contain only letters, numbers and underscores.") })
	email = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Email address"))
	password1 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=_("Password"))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=_("Password (again)"))
 
	def clean_username(self):
		try:
			user = User.objects.get(username__iexact=self.cleaned_data['username'])
		except User.DoesNotExist:
			return self.cleaned_data['username']
		raise forms.ValidationError(_("The username already exists. Please try another one."))
 
	def clean(self):
		if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
			if self.cleaned_data['password1'] != self.cleaned_data['password2']:
				raise forms.ValidationError(_("The two password fields did not match."))
		return self.cleaned_data


# class NameForm(forms.Form):
# 	title = forms.CharField(label='Title', max_length=100)
# 	description = forms.CharField(
# 		widget=forms.Textarea(attrs={'placeholder': 'Please enter the description'}))
# 	photo = forms.CharField(label='Photo', max_length=1000)

# 	def __init__(self, *args, **kwargs):
# 		extra_fields = kwargs.pop('extra', 0)

# 		if not extra_fields:
# 			extra_fields = 0

# 		super(NameForm, self).__init__(*args, **kwargs)
# 		#self.fields['total_input_fields'].initial = extra_fields

# 		for index in range(int(extra_fields)):
# 			self.fields['extra_field_{index}'.format(index=index)] = forms.CharField()
