#views.py
import logging

from .forms import *
from .models import AuctionItem, AuctionList

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.core.context_processors import csrf
from django.forms.formsets import formset_factory, BaseFormSet
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response, get_object_or_404, get_list_or_404
from django.template import RequestContext  # For CSRF



# from dynamicform.todo.forms import *





def home(request):
	return render(request,'welcome.html')


@login_required
def auction_create(request):
	# This class is used to make empty formset forms required
	# See http://stackoverflow.com/questions/2406537/django-formsets-make-first-required/4951032#4951032
	class RequiredFormSet(BaseFormSet):
		def __init__(self, *args, **kwargs):
			super(RequiredFormSet, self).__init__(*args, **kwargs)
			for form in self.forms:
				form.empty_permitted = False
	AuctionItemFormSet = formset_factory(AuctionItemForm, max_num=10, formset=RequiredFormSet)
	if request.method == 'POST': # If the form has been submitted...
		auction_list_form = AuctionListForm(request.POST) # A form bound to the POST data
		# Create a formset from the submitted data
		auction_item_formset = AuctionItemFormSet(request.POST, request.FILES)

		if auction_list_form.is_valid() and auction_item_formset.is_valid():
			auction_list = auction_list_form.save()
			for form in auction_item_formset.forms:
				auction_item = form.save(commit=False)
				auction_item.list = auction_list
				auction_item.save()
			return HttpResponseRedirect('/') # Redirect to a 'success' page
	else:
		auction_list_form = AuctionListForm()
		auction_item_formset = AuctionItemFormSet()

	# For CSRF protection
	# See http://docs.djangoproject.com/en/dev/ref/contrib/csrf/ 
	c = {'auction_list_form': auction_list_form,
		 'auction_item_formset': auction_item_formset,
		}
	c.update(csrf(request))

	return render(request,'auction/create.html', c)



@login_required
def auction_update(request, id=None):
	instance = get_object_or_404(AuctionList, id=id)
	auction_list_form = AuctionListForm(request.POST or None, instance = instance)
	if auction_list_form.is_valid():
		instance = auction_list_form.save(commit=False)
		instance.save()
		# messages.success(request, "Item Saved")
		return HttpResponseRedirect('/auction/detail/%s/'%id)

	context = {
		"title": instance.title,
		"instance": instance,
		"auction_list_form": auction_list_form		
	}
	return render(request, "auction/update.html", context)

@login_required
def auction_detail(request, id):

	instance = get_object_or_404(AuctionList, id=id)

	photos = AuctionItem.objects.filter(list_id=id)
	# for photo in photos:
	# 	logging.debug(photo.photo)

	context = {
		"photos": photos,
		"instance": instance
	}

	return render(request, "auction/detail.html", context)

@login_required
def auction_list(request):
	queryset_list = AuctionList.objects.all()

	context = {
		"object_list": queryset_list,
		"title": "List"
	}
	return render(request, "auction/list.html", context)

@login_required
def auction_delete(request, id=None):
	instance = get_object_or_404(AuctionList, id=id)
	photos = AuctionItem.objects.filter(list_id=id)
	instance.delete()
	photos.delete()
	return redirect("/")


# def home(request):
# 	if request.method == 'POST':
# 		form = NameForm(request.POST, extra=request.POST.get('extra_field_count'))
# 		if form.is_valid():
# 			c = {
# 				"form_data": form.cleaned_data,
# 				"loop_times": range(1, request.POST.get('extra_field_count')),
# 			}
# 			return render(request, "name.html", c)
# 	else:
# 		form = NameForm()
# 	context = {
# 		"title": "title",
# 		"form": form
# 	}
# 	return render(request, "index.html", context)

def about(request):
	return render_to_response('about.html')

def loggedin(request):
	context = {
		"username": request.user.username,
	}
	return render(request, "registration/loggedin.html", context)

def interview(request):
	QuestionFormSet = formset_factory(QuestionForm, extra=5)
	if request.method == "POST":
		formset = QuestionFormSet(request.POST)

		if formset.is_valid():
			message = "Thank you"
			for form in formset:
				logging.debug(form)
		else:
			message = "something went wrong"
		c = {
			"message": message
		}
		return render(request, "interview.html", c)
	else:
		c = {
			"formset": QuestionFormSet(),
		}
		return render(request, "interview.html", c)	

def register(request):
	if request.user.is_authenticated():
		messages.add_message(request, messages.INFO, 'You are already registered.')
		return render_to_response( 'message.html',
								{
									"title": "tytul",
								},
								context_instance=RequestContext(request))


	if request.method == 'POST':
		form = RegistrationForm(request.POST or None)
		if form.is_valid():
			user = User.objects.create_user(
			username=form.cleaned_data['username'],
			password=form.cleaned_data['password1'],
			email=form.cleaned_data['email']
			)
			return HttpResponseRedirect('/accounts/register/complete')
	else:
		form = RegistrationForm()
	c = {
		"form": form,
	}
	return render(request, 'registration/registration_form.html', c)

def registration_complete(request):
	return render(request, 'registration/registration_complete.html')
