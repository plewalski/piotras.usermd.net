# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20160827_0645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auctionlist',
            name='grid',
            field=models.IntegerField(default=2, choices=[(1, b'1'), (2, b'1-1'), (3, b'1-1-1')]),
        ),
    ]
