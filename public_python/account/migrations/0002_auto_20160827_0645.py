# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auctionlist',
            name='grid',
            field=models.IntegerField(default=2, choices=[(1, b'jeden'), (2, b'dwa'), (3, b'trzy')]),
        ),
    ]
